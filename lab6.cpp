/**************************
 *Jaden Naylor 
 *CPSC 1021, 004, F20
 *jadenn@clemson.edu
 *Elliot McMillan 
 **************************/


#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;

typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}

int main(int argc, char const *argv[]) {  
  // IMPLEMENT as instructed below  
  /*This is to seed the random generator */  
  srand(unsigned (time(0)));  
  
  /*Create an array of 10 employees and fill information from standard input with prompt messages*/  
  	employee empArr[10];
	employee *startPtr, *endPtr;
	int numEmp;

	startPtr = &empArr[0]; // points to beginning of array
	endPtr = &empArr[10]; // points to end of array

	cout << "How many employees: " << endl; // gets number of employees 
	cin >> numEmp;

	for(int i = 0; i < numEmp; i++) { // loop to fill array 
		cout << "Last name: " << endl;
		cin >> empArr[i].lastName;
		cout << "First name: " << endl;
		cin >> empArr[i].firstName;
		cout << "Birth year: " << endl;
		cin >> empArr[i].birthYear;
		cout << "Hourly wage: " << endl; 
		cin >> empArr[i].hourlyWage;
	}

  /*After the array is created and initialzed we call random_shuffle() see the *notes to determine the parameters to pass   in.*/  
  random_shuffle(startPtr, endPtr, myrandom);	

  
  /*Build a smaller array of 5 employees from the first five cards of the array created    *above*/ 
	employee arr[5];	
  
  /*Sort the new array.  Links to how to call this function is in the specs     *provided*/    
  sort(startPtr, endPtr, name_order);
  
  /*Now print the array below */  
	for(int i = 0; i < 5; ++i) { // loop to print smaller sorted array
		cout << setw(5) << empArr[i].lastName;
		cout << ", ";
		cout << empArr[i].firstName << endl;
		cout << setw(5) << empArr[i].birthYear << endl;
		cout << setw(5) << setprecision(2) << fixed << empArr[i].hourlyWage << endl;
	}

	
  
  
  return 0;
}

/*This function will be passed to the sort funtion. Hints on how to implement* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {  
  // IMPLEMENT
	for(int i = 0; i < 5; i++) {
		if(lhs.lastName < rhs.lastName) {
			return true;
		}
			
		else {
		        return false;
        	}
	}

}
